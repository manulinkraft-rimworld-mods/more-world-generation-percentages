# More world generation percentages

a rimworld mod for more choices of world coverage in the world generation page, 
as seen on the [steam workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2781329699).

All details you want to know are on the steam workshop page: usage, tips, version support, ...

**development is slow**: I have other priorities, but I still want to keep this working. Feature request may get ignored for many weeks or months, but I will try to fix game-breaking bugs if they come up.

### Installation

This repository contains both the source code and the compiled mod. To install it:
* from this website
    1. click on the button with the arrow on the left of `Clone`
    2. choose "zip" and download it
    3. open it and copy the folder `more-world-generation-percentages` to your rimworld mods folder
* or from the command line, assuming you have `git`
    1. go into your rimworld mods folder
    2. open a terminal
    3. run `git clone https://gitlab.com/manulinkraft-rimworld-mods/more-world-generation-percentages.git`

### Contributing

I'm open to contributions, open an issue and tell me your ideas.
Or better, open a pull request (merge request?) and I'll include your contribution if it's good.

### License

I don't care, you're free to do whatever you want with this. 
Therefore, public domain, or CC0, or WTFPL, or whatever you call it.
I would appreciate getting mentioned/thanked when you use all or part of my project, though.

### Developer notes

Development only happens for the newest Rimworld game version, old game versions keep using an archived version of this mod.