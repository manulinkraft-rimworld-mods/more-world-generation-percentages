using System;
using System.Reflection;
using Verse;

namespace MoreWorldGenPercentages
{
	public class MoreWorldGenPercentages : Mod
	{
		/// <summary> Overwrites 2 fields in <see cref="RimWorld.Page_CreateWorldParams"/>
		/// so that we can choose more % in the dropdown menu while creating the world
		/// </summary>
		public MoreWorldGenPercentages(ModContentPack content) : base(content)
		{
			// Log.Message("I run when the game loads this mod");

			var coveragesArray = typeof(RimWorld.Page_CreateWorldParams)
				.GetField("PlanetCoverages", BindingFlags.Static | BindingFlags.NonPublic);

			var coveragesArrayDev = typeof(RimWorld.Page_CreateWorldParams)
				.GetField("PlanetCoveragesDev",
					BindingFlags.Static | BindingFlags.NonPublic);

			if (coveragesArray == null || coveragesArrayDev == null)
			{
				Log.Warning(
					"[MoreWorldGenPercentages] Arrays not found. The mod will have no effect");
				return;
			}

			float[] newCoverages = new[] {
				0.05f,  // will show as "5% (dev)"
                0.10f,
				0.12f,
				0.15f, // this setting shows a glitch in the world map
                0.20f,
				0.25f,
				0.30f,
				0.50f,
				1.00f
			};

			try
			{
				coveragesArray.SetValue(null, newCoverages);
				coveragesArrayDev.SetValue(null, newCoverages);
			}
			catch (Exception)
			{
				Log.Warning("[MoreWorldGenPercentages] Can't overwrite arrays. The mod will have no effect, but there will be no errors in the game");
			}
		}
	}
}